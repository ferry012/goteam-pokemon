
import axios from 'axios';
import store from '../store'
import router from "../routes";
import Vue from 'vue';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.baseURL = window.location.origin;
axios.defaults.withCredentials = true;

axios.interceptors.response.use(response => response, error => {
    const { status } = error.response

    console.log(status);
    // if (status >= 500) {
    //
    //         Vue.swal({
    //             icon: 'error',
    //             title: 'Error 500',
    //             text: 'Internal Server Error!',
    //             reverseButtons: true,
    //             confirmButtonText: 'Ok',
    //             cancelButtonText: 'Cancel'
    //         })
    //
    // }

    if (status === 401 && store.getters['auth/check']) {
        Vue.swal({
            icon: 'warning',
            title: 'Token Expired',
            text: 'token expired',
            reverseButtons: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            store.commit('auth/LOGOUT')
            router.push({ name: 'login' })
        })
    }

    if (status === 403) {
        Vue.swal({
            icon: 'error',
            title: 'Error 403 Unauthorized',
            text: 'You don\'t have permission to proceed this action.',
            reverseButtons: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.value) {
                router.go(-1);
            }

        })
    }

    return Promise.reject(error)
})



export default axios;
