import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {
    all_role: [],
    roles: {},
    page: {},
    search:'',
    sortBy:'created_at',
    sortDir:'desc',
    perPage:5,
    role:new Form({
        name:null,
        permissions:[]
    }),
    role_id:null,

};
const getters ={
    all_role: state => state.all_role,
    roles: state => state.roles,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,
    role: state => state.role,
    role_id: state => state.role_id,

};

const actions = {
    async getRole({commit,state}){
        try {
            const uri = `/api/roles/${state.role_id}/edit`;
            const { data } = await axios.get(uri);
            commit(types.ROLE, data.data )
        } catch (e) {
            console.log(e);
        }
    },
    async fetchRoleForDatatable ({commit,state}) {
        try {
            const uri = '/api/roles?page=' + state.page + '&search=' + state.search + '&sortby=' + state.sortBy + '&sortdir=' + state.sortDir + '&currentpage=' + state.perPage;
            const { data } = await axios.get(uri);
            commit(types.ROLES, data )

        } catch (e) {
            console.log(e);
        }
    },

    createRole ({commit,state}) {
        return new Promise((res,rej)=>{
            state.role.post('/api/roles')
                .then((response) => {
                    commit(types.ROLE_CLEAR )
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    updateRole ({commit,state}) {
        return new Promise((res,rej)=>{
            state.role.patch(`/api/roles/${state.role_id}`)
                .then((response) => {
                    commit(types.ROLE_CLEAR )
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    async getRoles({commit, state}) {
        try {
            const uri = `/api/roles?getAll=true`;
            const {data} = await axios.get(uri);
            commit(types.ALL_ROLE, data.data)
        } catch (e) {
            console.log(e);
        }
    },

};

const mutations = {
    [types.ROLE_CLEAR] (state ) {
        state.role.clear();
        state.role.reset();
    },
    [types.ROLES] (state, roles ) {
        state.roles = roles
    },
    [types.ROLE] (state, role ) {

        state.role.fill(role)
    },
    [types.ROLE_PAGE] (state, page ) {
        state.page = page
    },
    [types.ROLE_SEARCH] (state,  search ) {
        state.search = search
    },
    [types.ROLE_SORTBY] (state,  sortBy ) {
        state.sortBy = sortBy
    },
    [types.ROLE_SORTDIR] (state,  sortDir ) {
        state.sortDir = sortDir
    },
    [types.ROLE_PERPAGE] (state,  perPage ) {
        state.perPage = perPage
    },
    [types.ROLE_CREATE] (state,  role ) {
        state.role = role
    },
    [types.ROLE_UPDATE] (state,  role ) {
        state.role = role
    },
    [types.ROLE_ID] (state,  id ) {
        state.role_id = id
    },
    [types.ALL_ROLE](state, roles) {
        state.all_role = roles
    },


};

export  {
    state,
    getters,
    actions,
    mutations
};
