import axios from 'axios';
import * as types from '../mutation-types'
import router from "../../routes";
import {Form} from "vform";

const state = {
    isLoggedIn: isLoggedIn(),
    user:new Form({
        id:null,
        name:null,
        email:null,
        username:null,
        password:null,
        password_confirmation:null
    }),

    roles:isRoles(),
    permissions:isPermissions(),
};

const getters ={
    user: state => state.user,
    roles: state => state.roles,
    permissions: state => state.permissions,
    check: state => state.isLoggedIn
};

const actions = {
    setLogin ({ commit, dispatch }) {
        commit(types.SET_LOGIN)
    },

    async fetchUser ({ commit }) {
        try {
            const { data } = await axios.get('/api/user')

            commit(types.FETCH_USER_SUCCESS, { user: data })
        } catch (e) {
            commit(types.FETCH_USER_FAILURE)
        }
    },

    updateUser ({commit, state}) {
        return new Promise((res, rej) => {
            state.user.patch(`/api/profile-update`)
                .then((response) => {
                    commit(types.FETCH_USER_SUCCESS,{ user: response.data })
                    res(response.data);
                }).catch((err) => {
                rej(err)
            })
        })

    },

    async logout ({ commit }) {
        try {
            await axios.post('/logout')
            commit(types.LOGOUT)
        } catch (e) { }


    },

    async fetchAbilities({commit}){
        try {
            const { data } = await axios.get('/api/abilities')
            commit(types.FETCH_ABILITIES, { abilities: data.data })
        } catch (e) {
            commit(types.FETCH_USER_FAILURE)
        }
    }
};

const mutations = {
    [types.SET_LOGIN] (state) {
        localStorage.setItem('isLoggedIn', 'true')
        state.isLoggedIn = true
    },

    [types.FETCH_USER_SUCCESS] (state, { user }) {
        state.user.fill(user)
    },

    [types.FETCH_USER_FAILURE] (state) {
        localStorage.removeItem('isLoggedIn')
        localStorage.removeItem('permissions')
        localStorage.removeItem('roles')
        state.roles = null;
        state.permissions = null;
        state.isLoggedIn = null;
    },

    [types.LOGOUT] (state) {
        localStorage.removeItem('isLoggedIn')
        localStorage.removeItem('permissions')
        localStorage.removeItem('roles')
        state.user.clear();
        state.user.reset();
        state.roles = null;
        state.permissions = null;
        state.isLoggedIn = null;

    },

    [types.UPDATE_USER] (state,  user ) {
        state.user = user
    },
    [types.FETCH_ABILITIES] (state, { abilities }) {
        localStorage.setItem('permissions',JSON.stringify(Object.assign(abilities.permissions)))
        localStorage.setItem('roles',JSON.stringify(Object.assign(abilities.roles)))
        state.roles = abilities.roles;
        state.permissions = abilities.permissions;
    },

};

export  {
    state,
    getters,
    actions,
    mutations
};



function isLoggedIn() {
    const isLoggedIn = localStorage.getItem("isLoggedIn");
    if (!isLoggedIn) {
        return null;
    }

    return isLoggedIn;
}

function isRoles() {
    const roles = localStorage.getItem("roles");
    if (!roles) {
        return [];
    }

    return roles;
}

function isPermissions() {
    const permissions = localStorage.getItem("permissions");
    if (!permissions) {
        return [];
    }

    return permissions;
}
