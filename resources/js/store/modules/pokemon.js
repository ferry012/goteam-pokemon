import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";
import {
    POKEMON,
    POKEMON_DISLIKE,
    POKEMON_LIKE,
    POKEMON_LOAD,
    POKEMON_SEARCH,
    POKEMON_STATUS, POKEMON_THROW_ERR,
    POKEMON_USER_ID
} from "../mutation-types";


const state = {
    pokemon: {},
    offset: 0,
    search: '',
    like_pokemon: {},
    dislike_pokemon: null,
    user_id: null,
    err: null,
};
const getters = {
    pokemon: state => state.pokemon,
    offset: state => state.offset,
    search: state => state.search,
    like_pokemon: state => state.like_pokemon,
    dislike_pokemon: state => state.dislike_pokemon,
    user_id: state => state.user_id,
    err: state => state.err,
};

const actions = {
    async fetchPokemonForDatatable({commit, state}) {
        try {
            const uri = '/api/pokemon/details';
            const {data} = await axios.get(uri);
            commit(types.POKEMON, data)

        } catch (e) {
            // commit(types.POKEMON_THROW_ERR, e)
        }
    },
    async loadThisPokemon({commit, state}) {
        console.log('loadThisPokemon');
        try {
            const uri = '/api/pokemon/details?loadMore=true' + '&dataCount=' + state.offset;
            const {data} = await axios.get(uri);
            commit(types.POKEMON, data)
        } catch (e) {
            console.log(e);
        }
    },

    async searchPokemon({commit, state}) {
        try {
            const uri = '/api/pokemon/details?search=' + state.search;
            const {data} = await axios.get(uri);
            commit(types.POKEMON, data)
        } catch (e) {
            console.log(e);
        }
    },
};

const mutations = {
    [types.POKEMON](state, pokemon) {
        state.pokemon = pokemon
    },
    [types.POKEMON_LOAD](state, offset) {
        state.offset = offset
    },
    [types.POKEMON_SEARCH](state, search) {
        state.search = search
    },
    [types.POKEMON_THROW_ERR](state, err) {
        state.err = err
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
