/*auth.js*/
export const LOGOUT = 'LOGOUT'
export const SET_LOGIN = 'SET_LOGIN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'
export const FETCH_ABILITIES = 'FETCH_ABILITIES'

/*page.js*/
export const PAGE_NAME = 'PAGE_NAME'

/*permission*/
export const PERMISSIONS = 'PERMISSIONS'
export const PERMISSION_PAGE = 'PERMISSION_PAGE'
export const PERMISSION_SEARCH = 'PERMISSION_SEARCH'
export const PERMISSION_SORTBY = 'PERMISSION_SORTBY'
export const PERMISSION_PERPAGE = 'PERMISSION_PERPAGE'
export const PERMISSION_SORTDIR = 'PERMISSION_SORTDIR'
export const PERMISSION_CREATE = 'PERMISSION_CREATE'
export const PERMISSION_CLEAR = 'PERMISSION_CLEAR'
export const PERMISSION_ID = 'PERMISSION_ID'
export const PERMISSION = 'PERMISSION'
export const PERMISSION_UPDATE = 'PERMISSION_UPDATE'
export const ALL_PERMISSION = 'ALL_PERMISSION'

/*role*/
export const ROLES = 'ROLES'
export const ROLE_PAGE = 'ROLE_PAGE'
export const ROLE_SEARCH = 'ROLE_SEARCH'
export const ROLE_SORTBY = 'ROLE_SORTBY'
export const ROLE_PERPAGE = 'ROLE_PERPAGE'
export const ROLE_SORTDIR = 'ROLE_SORTDIR'
export const ROLE_CREATE = 'ROLE_CREATE'
export const ROLE_CLEAR = 'ROLE_CLEAR'
export const ROLE_ID = 'ROLE_ID'
export const ROLE = 'ROLE'
export const ROLE_UPDATE = 'ROLE_UPDATE'
export const ALL_ROLE = 'ALL_ROLE'

/*users*/
export const USERS= 'USERS'
export const USER_PAGE = 'USER_PAGE'
export const USER_SEARCH = 'USER_SEARCH'
export const USER_SORTBY = 'USER_SORTBY'
export const USER_PERPAGE = 'USER_PERPAGE'
export const USER_SORTDIR = 'USER_SORTDIR'
export const USER_CREATE = 'USER_CREATE'
export const USER_CLEAR = 'USER_CLEAR'
export const USER_ID = 'USER_ID'
export const USER = 'USER'
export const USER_UPDATE = 'USER_UPDATE'
export const ALL_USER = 'ALL_USER'

/*pokemon*/
export const POKEMON= 'POKEMON'
export const POKEMON_LOAD = 'POKEMON_LOAD';
export const POKEMON_SEARCH = 'POKEMON_SEARCH';
export const POKEMON_LIKE = 'POKEMON_LIKE';
export const POKEMON_DISLIKE = 'POKEMON_DISLIKE';
export const POKEMON_USER_ID = 'POKEMON_USER_ID';
export const POKEMON_THROW_ERR = 'POKEMON_THROW_ERR';

/*register-user*/
export const REGISTER_USER= 'REGISTER_USER';
export const REGISTER_USER_CLEAR= 'REGISTER_USER_CLEAR';
export const REGISTER_USER_CREATE= 'REGISTER_USER_CREATE';
