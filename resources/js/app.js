
require('./bootstrap');

import Vue from 'vue';
import Main from './components/Main';
import Vuex, {mapGetters} from 'vuex';
import router from './routes';
import store from './store';
import Permissions from './mixins/Permissions';
import Roles from './mixins/Roles';
import VuePromiseBtn from 'vue-promise-btn';
import 'vue-promise-btn/dist/vue-promise-btn.css';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import { BootstrapVue } from 'bootstrap-vue'
import VueColumnsResizable from 'vue-columns-resizable';

Vue.use(Vuex);
Vue.mixin(Permissions);
Vue.mixin(Roles);
Vue.use(VuePromiseBtn);
Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueColumnsResizable);


const app = new Vue({
    el: '#app',
    render:h =>h(Main),
    router,
    store,
    components:{
        Main,
    },

    watch:{
        $route:{
            async  handler(){
                if(this.$route.meta.requiresAuth){
                    await this.$store.dispatch('auth/fetchUser');
                    await this.$store.dispatch('auth/fetchAbilities');
                }

            },
            immediate:true
        }
    },
});
