<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/test1', function() {
//    $apiurl = "https://pokeapi.co/api/v2/" . "pokemon-species/" . 10;
//    $apibody = [
//        'Content-Type' => 'application/json',
//        'http_errors' => false,
//    ];
//    $client = new \GuzzleHttp\Client();
//    $response = $client->get($apiurl,$apibody);
//    $data = json_decode($response->getBody()->getContents(),true);
//    $description = '';
//    foreach($data['flavor_text_entries'] as $key => $value) {
//        if($value['language']['name'] === 'en'){
//            $description = str_replace("\f", "", $value['flavor_text']);
//            break;
//        }
//    }
//    dd('JAY',$description);
//});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/{any}',[\App\Http\Controllers\AppController::class,'index'])->where('any','.*');
Auth::routes();
