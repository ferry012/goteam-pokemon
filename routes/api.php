<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {

    Route::get('abilities', [\App\Http\Controllers\Api\AbilitiesController::class, 'index']);
    Route::patch('profile-update', [\App\Http\Controllers\Api\AbilitiesController::class, 'updateProfile']);
    Route::resource('permissions', \App\Http\Controllers\Api\PermissionController::class);
    Route::resource('roles', \App\Http\Controllers\Api\RoleController::class);
    Route::resource('users', \App\Http\Controllers\Api\UserController::class);
    Route::get('pokemon/details',[\App\Http\Controllers\Api\PokemonController::class, 'pokeIndex']);
    Route::post('pokemon/{id}/status/like',[\App\Http\Controllers\Api\PokemonController::class, 'likeStatus']);
    Route::post('pokemon/{id}/status/dislike',[\App\Http\Controllers\Api\PokemonController::class, 'dislikeStatus']);
    Route::get('pokemon/{id}/status-all',[\App\Http\Controllers\Api\PokemonController::class, 'getStatus']);
    Route::delete('pokemon/{id}/remove-status',[\App\Http\Controllers\Api\PokemonController::class, 'removeStatus']);
    Route::get('pokemon/{id}/user-list',[\App\Http\Controllers\Api\PokemonController::class, 'getUserListPokemon']);
});


Route::post('register', [\App\Http\Controllers\Api\UserController::class, 'register']);

