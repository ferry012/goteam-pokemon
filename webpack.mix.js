const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/app.js', 'public/js').version()
    // .sass('resources/sass/app.scss', 'public/css').version()
    .postCss('resources/css/app.css', 'public/css').version()
    .copyDirectory('resources/img/','public/images')
    .webpackConfig({
        output: { chunkFilename: 'js/chunks/[name].js?id=[chunkhash]' },
    });


mix.scripts([
    'resources/js/core/jquery.min.js',
    'resources/js/core/popper.min.js',
    'resources/js/core/bootstrap-material-design.min.js',
    'resources/js/plugins/perfect-scrollbar.jquery.min.js',
    'resources/js/material-dashboard.js',
], 'public/js/mix-scripts.js')

mix.browserSync('http://goteam-pokemon.test/');
