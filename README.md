# GoTeam Pokemon Assessment
*****
### Getting started on local development

 Create .env file for Laravel environment (or copy from .env.example) using this command below
> cp .env.example .env

Install dependencies and packages

> composer install and npm install

config your sanctum database connection and SANCTUM_STATEFUL_DOMAINS in env file [very important]

> SANCTUM_STATEFUL_DOMAINS=goteam-pokemon.test,localhost:3000,127.0.0.1:8000,localhost:8000 

Run database migration

> php artisan migrate

Run database seeder
> php artisan db:seed

After that optimize cache
> php artisan optimize:clear

Lastly run on development stage
> npm run dev

**Credentials**

>_default superadmin_ 
> 
>user:sa@test.com \
>pass: password


>_default admin_\
>user : admin@test.com\
>pass : password
