<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasFactory, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:M d, Y',
//        'birthday' => 'datetime:M d, Y',
    ];


    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public static function UserWithRoles($id)
    {
        $user = User::with(array('roles' => function ($query) {
            $query->select('name');
        }))->find($id);
        $map['id'] = $user->id;
        $map['name'] = $user->name;
        $map['username'] = $user->username;
        $map['email'] = $user->email;
        $map['birthday'] = $user->birthday;
        $map['roles'] = $user->roles->map(function ($user) {
            return $user->name;
        });

        return $map;
    }

    public function like(){
        return $this->hasMany(LikeStatus::class, 'user_id', 'id');
    }

    public function dislike(){
        return $this->hasMany(DislikeStatus::class, 'user_id', 'id');
    }
}
