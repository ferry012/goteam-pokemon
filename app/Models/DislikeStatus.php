<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DislikeStatus extends Model
{
    use HasFactory;

    protected $table = 'dislike_status';
}
