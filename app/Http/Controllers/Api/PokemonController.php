<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DislikeStatus;
use App\Models\LikeStatus;
use App\Models\User;
use App\Services\PokemonApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PokemonController extends Controller
{
    private $pokemonApiService;

    public function __construct(PokemonApiService $pokemonApiService)
    {
        $this->pokemonApiService = $pokemonApiService;
    }

    public function pokeIndex(Request $request){
        $search = $request->search;
        if($search){
            $dataSearch = $this->pokemonApiService->searchPokemon($search);
            $data = [];
            if($dataSearch){
                $data[0] = [
                    'id' => $dataSearch['id'],
                    'name' => $dataSearch['name'],
                    'images' => $dataSearch['sprites']['other']['dream_world']['front_default'],
                    'description' => $this->pokemonApiService->pokemonDescription($dataSearch['id']),
                ];
            }

            return response()->json($data);
        }

        $pokemon = $this->pokemonApiService->getAllPokemon();
        if($pokemon){
            $pokemon = $this->index($pokemon);
            //this will load more Pokémon data in 12 offset interval
            if($request->loadMore){
               $dataOffset = $request->dataCount >= 12 ? 12 : 0;
               $additionalPokemon = $this->pokemonApiService->getAllPokemon($dataOffset,$request->dataCount);
               $pokemon = array_merge($pokemon,$additionalPokemon);
                for ($p = 0; $p < count($pokemon); $p++){
                    if($p >= 12){
                        $pokemon = $this->getPokemon($pokemon, $p);
                    }
                }
            }
            return response()->json($pokemon);
        }
    }

    protected function index($pokemon){
        for ($p = 0; $p < count($pokemon); $p++){
            $pokemon = $this->getPokemon($pokemon, $p);
        }
        return $pokemon;
    }

    protected function indexLike($like)
    {
        for ($l = 0; $l < count($like); $l++){
            $like = $this->getLikeData($like, $l);
        }
        return $like;
    }

    protected function indexDislike($dislike)
    {
        for ($d = 0; $d < count($dislike); $d++){
            $dislike = $this->getDislikeData($dislike, $d);
        }
        return $dislike;
    }

    /**
     * @param $pokemon
     * @param int $p
     * @return array
     */
    protected function getPokemon($pokemon, int $p): array
    {
        $description = $this->pokemonApiService->pokemonDescription($this->pokemonApiService->getPokemonID($pokemon[$p]['url']));
        $image = $this->pokemonApiService->pokemonImages($pokemon[$p]['url']);
        $pokemon[$p]['images'] = $image['sprites']['other']['dream_world']['front_default'];
        $pokemon[$p]['description'] = $description;
        $pokemon[$p]['id'] = $this->pokemonApiService->getPokemonID($pokemon[$p]['url']);
        return $pokemon;
    }

    /**
     * @param $like
     * @param int $l
     * @return array
     */
    protected function getLikeData($like, int $l): array
    {
        $image = $this->pokemonApiService->pokemonImages(env('API_POKEAPI') . 'pokemon/' . $like[$l]['like_id'] . '/');
        $like[$l]['images'] = $image['sprites']['other']['dream_world']['front_default'];
        $like[$l]['name'] = $image['name'];
        return $like;
    }

    /**
     * @param $dislike
     * @param int $d
     * @return array
     */
    protected function getDislikeData($dislike, int $d): array
    {
        $image = $this->pokemonApiService->pokemonImages(env('API_POKEAPI') . 'pokemon/' . $dislike[$d]['dislike_id'] . '/');
        $dislike[$d]['images'] = $image['sprites']['other']['dream_world']['front_default'];
        $dislike[$d]['name'] = $image['name'];
        return $dislike;
    }



    public function getStatus($id){
        $like = LikeStatus::where('user_id',$id)->get();
        $dislike = DislikeStatus::where('user_id',$id)->get();

        $like = $this->indexLike($like->toArray());
        $dislike = $this->indexDislike($dislike->toArray());

        $data = [
            'pokemon_like' => $like,
            'pokemon_dislike' => $dislike,
        ];

        return response()->json($data);
    }

    public function likeStatus(Request $request,$id){
        return DB::table('like_status')->insert([
            'user_id' => (int)$id,
            'like_id' => $request->all()['like_id']
        ]);
    }

    public function dislikeStatus(Request $request,$id){
        return DB::table('dislike_status')->insert([
            'user_id' => (int)$id,
            'dislike_id' => $request->all()['dislike_id']
        ]);
    }

    public function removeStatus(Request $request,$id){
        if($request->like){
           return DB::table('like_status')->where('id',$id)->delete();
        } else{
            return DB::table('dislike_status')->where('id',$id)->delete();
        }
    }

    public function getUserListPokemon($id){
        $user_list = User::with('like','dislike')
                        ->whereNotIn('users.id',[$id])
                        ->get();
        $user_list_arr = $user_list->toArray();
        foreach ($user_list->toArray() as $key => $list){
            $user_list_arr[$key]['like'] = $this->indexLike($list['like']);
            $user_list_arr[$key]['dislike'] = $this->indexDislike($list['dislike']);
        }
        return response()->json($user_list_arr);
    }
}
