<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Interfaces\RegisterServiceInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return UserResource
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('user_view') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        if($request->getAll) {
            $query = User::with('roles')->orderBy('name','asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = intVal($request->currentpage);
            $superadmin = User::role('super_administrator')->pluck('id');
                //super admin excluded in the table
            $query = User::with('roles')->where('name', 'LIKE', "%$searchValue%")
                ->where(function($q) use($superadmin){
                    $q->whereNotIn('id',[$superadmin]);
                })
                ->where(function($q) use($searchValue){
                    $q->orwhere('email', 'LIKE', "%$searchValue%")
                        ->orwhereDate('created_at', 'LIKE', "%$searchValue%");
                })
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }

        return new UserResource($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        abort_if(Gate::denies('user_create') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255','unique:users','regex:/(^[A-Za-z0-9-_]+$)+/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'roles' => ['required'],
            'birthday' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::create($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        if($user){
            return  response()->json($user);
        }
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255','unique:users','regex:/(^[A-Za-z0-9-_]+$)+/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::create($request->all());
        $roles = app()->make(RegisterServiceInterface::class)->getGuestRole() ? app()->make(RegisterServiceInterface::class)->getGuestRole() : [];
        $user->assignRole($roles);

        if($user){
            return  response()->json($user);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return UserResource
     */
    public function edit($id)
    {
        abort_if(Gate::denies('user_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserResource(User::UserWithRoles($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        abort_if(Gate::denies('user_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required',
            'username' => 'required|string|max:25|unique:users,username,'.$id.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'password' => 'sometimes|confirmed',
            'password_confirmation' => 'required_with:password|same:password',
            'roles' => 'required',
            'birthday' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->update($request->except('roles', 'password_confirmation'));
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        if ($user) {
            return  response()->json($user);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        abort_if(Gate::denies('user_delete') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = User::findorfail($id);
        $user->delete();


        return response()->json($user);
    }
}
