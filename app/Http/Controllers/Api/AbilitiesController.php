<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AbilityResource;
use App\Models\User;
use Illuminate\Http\Request;

class AbilitiesController extends Controller
{
    public function index(){
        $roles = auth()->user()->getRoleNames();

        $permissions = auth()->user()->getPermissionsViaRoles()
        ->pluck('name');

        $abilities['roles'] = $roles;
        $abilities['permissions'] = $permissions;

        return new AbilityResource($abilities);
    }


    public function updateProfile(Request $request){

        $user = auth()->user();

        $request->validate([
            'name' => 'required',
            'username' => 'required|string|max:25|unique:users,username,'.$user->id.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
            'password' => 'sometimes|confirmed',
            'password_confirmation' => 'required_with:password|same:password',
        ]);

        $user = User::findOrFail($user->id);
        $user->update($request->except('password_confirmation'));
        if ($user) {
            return  response()->json($user);
        }

    }
}
