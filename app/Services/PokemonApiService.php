<?php

namespace App\Services;


class PokemonApiService
{

    public function __construct()
    {
        $this->Endpoint = env('API_POKEAPI');
    }

    public function getAllPokemon($offsetCount = 0,$limitCount = 12){
//        $apiurl = $this->Endpoint . 'pokemon?limit=-1'; // show all
        // show by 12 in 4 pokemon in a row
        $apiurl = $this->Endpoint . "pokemon?offset=$offsetCount&limit=$limitCount";
        $apibody = [
            'Content-Type' => 'application/json',
            'http_errors' => false,
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $data = json_decode($response->getBody()->getContents(),true);

        return $data['results'] ?? null;
    }

    public function pokemonImages($url){
        $apibody = [
            'Content-Type' => 'application/json',
            'http_errors' => false,
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url,$apibody);
        $data = json_decode($response->getBody()->getContents(),true);

        return $data ?? null;
    }

    public function pokemonDescription($id){
        $apiurl = $this->Endpoint . "pokemon-species/" . $id;
        $apibody = [
            'Content-Type' => 'application/json',
            'http_errors' => false,
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $data = json_decode($response->getBody()->getContents(),true);
        $description = '';
        foreach($data['flavor_text_entries'] as $key => $value) {
            if($value['language']['name'] === 'en'){
                $description = str_replace("\f", "", $value['flavor_text']);
                break;
            }
        }
        return $description;
    }

    public function getPokemonID($url){
        $explode =  explode("/",$url);
        return (int)$explode[6];
    }

    public function searchPokemon($key){
        $apiurl = $this->Endpoint . "pokemon/" . $key;
        $apibody = [
            'Content-Type' => 'application/json',
            'http_errors' => false,
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $data = json_decode($response->getBody()->getContents(),true);

        return $data ?? null;
    }

}
